'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('histories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      game_id: {
        type: Sequelize.INTEGER
      },
      firstRound_Choice: {
        type: Sequelize.STRING
      },
      firstRound_Result: {
        type: Sequelize.STRING
      },
      secondRound_Choice: {
        type: Sequelize.STRING
      },
      secondRound_Result: {
        type: Sequelize.STRING
      },
      thirdRound_Choice: {
        type: Sequelize.STRING
      },
      thirdRound_Result: {
        type: Sequelize.STRING
      },
      final_Result: {
        type: Sequelize.STRING
      },
      score: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('histories');
  }
};