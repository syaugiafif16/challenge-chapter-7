'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.user, {
        foreignKey: 'user_id'
      }),
      this.belongsTo(models.room, {
        foreignKey: 'game_id'
      })
    }

  }
  history.init({
    user_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    game_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    firstRound_Choice: {
      type: DataTypes.STRING,
      allowNull: false
    },
    firstRound_Result: {
      type: DataTypes.STRING,
      allowNull: false
    },
    secondRound_Choice: {
      type: DataTypes.STRING,
      allowNull: false
    },
    secondRound_Result: {
      type: DataTypes.STRING,
      allowNull: false
    },
    thirdRound_Choice: {
      type: DataTypes.STRING,
      allowNull: false
    },
    thirdRound_Result: {
      type: DataTypes.STRING,
      allowNull: false
    },
    final_Result: {
      type: DataTypes.STRING,
      allowNull: false
    },
    score: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'history',
    tableName: 'histories'
  });
  return history;
};