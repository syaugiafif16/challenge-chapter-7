'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
    
      this.hasMany(models.room, {
        foreignKey: 'player1_id'
      }),
      this.hasMany(models.room, {
        foreignKey: 'player2_id'
      }),
      this.hasMany(models.history, {
        foreignKey: 'user_id'
      })
    }
  }
  user.init({
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'user',
    tableName: "users"
  });
  return user;
};