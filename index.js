
const express = require('express');
const app = express();
require('dotenv').config()
const path = require('path');
const port = process.env.port
const cors = require('cors')
const router = require('./router');


app.set('view engine','ejs');
app.use(cors())
app.use(express.json()); //ini adalah config untuk menerima request json dari client
app.use(express.urlencoded({extended:true})); //config untuk menerima request selain json
app.use(express.static(__dirname + '/public'));

app.use(router);//ROUTER
app.get('/',(req,res) =>{
    res.render('index');
})
app.get('/game',(req,res) =>{
    res.render('game');
})


app.listen(3000, () => {
    console.log(`server is running at ${3000}`)
})
