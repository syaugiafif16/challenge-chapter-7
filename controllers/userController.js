
const { user } = require('../models')
const bcrypt = require('bcrypt');
const { body, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');


module.exports = {
    register: async (req,res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }


            const user_data = await user.findOne({ where : {
                username: req.body.username
            }})

            if(user_data) {
                return res.json({
                    message : "Username sudah digunakan!"
                })
            }

            const user_email = await user.findOne({ where : {
                email: req.body.email
            }})

            if(user_email) {
                return res.json({
                    message : "Email sudah digunakan!"
                })
            }
            const password = bcrypt.hashSync(req.body.password, 10)
            const data = await user.create({
                username: req.body.username,
                email: req.body.email,
                password: password
            });

            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    login: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const user_data = await user.findOne({ where : {
                username: req.body.username
            }})

            if(!user_data) {
                return res.json({
                    message : "User does not exists!"
                })
            }

            if (bcrypt.compareSync(req.body.password, user_data.password)) {
                const token = jwt.sign(
                  { id: user_data.id },
                  'secret',
                  { expiresIn: '6h' }
                )
          
                return res.status(200).json({
                    token: token
                })
              }
            
            return res.json({
                message : "Wrong Password!"
            })
            
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    getProfile: async(req, res) => {
        try {
            const user_data = await user.findOne({ where : {
                id: res.user.id
            }})
            
            return res.json({
                data : user_data
            })
            
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    }






  
    // game: (req, res) => {
    //     return res.sendFile(path.join(__dirname, '../game.html'));
    // },
    // home: (req, res) => {
    //     return res.sendFile(path.join(__dirname, '../index.html'));
    // }





}