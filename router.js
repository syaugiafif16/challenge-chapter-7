const express = require('express');
const router = express.Router();
const userController = require('./controllers/userController')
const gameController = require('./controllers/gameController')
const userHistoryController = require('./controllers/userHistoryController')

const { body } = require('express-validator');
const checkToken = require('./middleware/checkToken');
const checkLogin = require('./middleware/checkLogin');




router.post('/register',body('email').isEmail(), body('username').notEmpty(),body('password').notEmpty(),userController.register);
router.post('/login', body('username').notEmpty(), body('password').notEmpty(), userController.login);
router.get('/user/getProfile',checkToken, userController.getProfile);

router.get('/game/room/list', checkToken, gameController.listRoom)
router.post('/game/room/create', checkToken, gameController.createRoom)
router.post('/game/room/join', checkToken, gameController.joinRoom)
router.post('/game/fight/:room_name/:room_code', checkToken, gameController.fight)







module.exports = router;